import java.util.Scanner;

public class Main {

    public static String reverseHyphens(String str) {
        var arr = str.split(" ");
        for (int i = 0; i < arr.length; i++) {
            if (arr[i].contains("-")) {
                var word = arr[i].split("-");
                arr[i] = String.join("-", word[1], word[0]);
            }
        }

        return String.join(" ", arr);
    }

    public static void main(String[] args) {
        var s = new Scanner(System.in);
        System.out.print("Enter a string: ");
        var str = s.nextLine();
        var result = reverseHyphens(str);
        System.out.println(result);
    }

}
