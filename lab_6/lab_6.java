import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) throws IOException {

        HashMap<Integer, Integer> hash = new HashMap<>();
        BufferedReader reader = new BufferedReader(new FileReader("C:\\file.txt"));

        while (true) {
            String line = reader.readLine();
            if (line == null) {
                break;
            }
            for (int i = 0; i < line.length(); i++) {
                char c = line.charAt(i);
                if (c != ' ') {
                    int value = hash.getOrDefault((int) c, 0);
                    hash.put((int) c, value + 1);
                }
            }
        }

        reader.close();

        for (int key : hash.keySet()) {
            System.out.println((char) key + ": " + hash.get(key));
        }
    }
}
