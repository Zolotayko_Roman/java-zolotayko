public class Main {
    public static void main(String[] args) {
        int sum = 0, count = 0;
        for (var arg : args) {
            int num = Integer.parseInt(arg);
            if (num % 2 != 0) {
                sum += num;
                count++;
                if (count > 1) {
                    break;
                }
            }
        }
        System.out.println(sum);
    }
}
