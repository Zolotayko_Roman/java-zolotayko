public class Main {

    public static int getPopularElement(int[] arr)
    {
        int count = 1, tempCount;
        int popular = arr[0];
        int temp = 0;
        for (int i = 0; i < (arr.length - 1); i++)
        {
            temp = arr[i];
            tempCount = 0;
            for (int j = 1; j < arr.length; j++) {
                if (temp == arr[j])
                    tempCount++;
            }
            if (tempCount > count) {
                popular = temp;
                count = tempCount;
            }
        }
        return popular;
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 1, 1, 8};
        int mostPopular = getPopularElement(arr);

        System.out.println(mostPopular);
    }

}
